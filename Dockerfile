# Version: 0.1

FROM ubuntu:14.04
MAINTAINER Moli "molisoft@qq.com"
RUN apt-get update
RUN apt-get -y install php5
RUN apt-get -y install ruby
RUN apt-get -y install wget

RUN apt-get -y install python-software-properties
RUN apt-get -y install golang

RUN export GOROOT=/usr/lib/go
RUN export GOBIN=/usr/bin/g

RUN apt-get update
RUN apt-get -y install python2.7

RUN apt-get -y install openjdk-7-jre
RUN apt-get -y install openjdk-7-jdk

RUN apt-get -y install g++


RUN wget http://git.oschina.net/moli/runcode/raw/master/run.rb -P /root/
